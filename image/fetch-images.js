const supabaseImage = require('../supabase/supabase-image');

const fetchImages = async (postId) => {
    
    try {
        const rawResponse = await supabaseImage.fetchImagesWithPostId(postId);
        
        let imageLinks = [];

        rawResponse.data.forEach(imageLinkData => {
            imageLinks.push(imageLinkData.link);    
        });

        return imageLinks;
    }catch(reason) {
        throw `failed to fetch images with post id: ${postId} due to ${reason}.`;
    }
}

module.exports.fetchImages = fetchImages;
