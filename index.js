const express = require('express');
const app = express();
const port = 8080;
const cors = require('cors');
const bodyParser = require('body-parser');
const PostEndpoints = require('./post/post-endpoints');
const Imgur = require('./imgur-api/imgur');
const Supabase = require('./supabase/supabase');
require('dotenv').config();


app.use(cors());
app.use(bodyParser.urlencoded({ extended: false, limit: '500mb' }));
app.use(bodyParser.json({limit: '500mb'}));

const initialize = async () => {
  await Supabase.initialize();
  await Imgur.initialize();

  PostEndpoints.initialize(app);
};

initialize();

app.listen(port, () => {
  console.log(`Running ENV: ${process.env.ENVIRONMENT} http://localhost:${port}`);
});


