const supabasePost = require('../supabase/supabase-post');
const { fetchImages } = require('../image/fetch-images');
const { buildOutput } = require('./post-output-builder');
const { StatusCodes } = require('http-status-codes');

const fetchPost = async (request, response) => {
    try {
        const {id} = request.query;
        const rawResponse = await supabasePost.fetchPostById(id);
        
        if(rawResponse.data.length < 1) {
            console.error(`Cannot find post with id: ${id}`);
            response.sendStatus(StatusCodes.NOT_FOUND);
            return;
        }

        const output = await _buildResponse(rawResponse);
        response.send(output);
    }catch(reason) {
        response.sendStatus(StatusCodes.BAD_REQUEST).send(reason);
        return;
    }
};

const _buildResponse = async (rawPostResponse) => {
    const post = rawPostResponse.data[0];
    const imageLinks = await fetchImages(post.id);
    const postOutput = buildOutput(post, imageLinks);    

    const response = {
        data: postOutput
    } 

    return response;
}

module.exports.fetchPost = fetchPost;