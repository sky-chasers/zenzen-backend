
const {createPost} = require('./create-post');
const {fetchPosts} = require('./fetch-posts');
const {fetchPost} = require('./fetch-post');

const initialize = (app) => {
    app.get('/post', fetchPost);
    app.post('/post', createPost);
    app.get('/posts', fetchPosts);
};

module.exports.initialize = initialize;
