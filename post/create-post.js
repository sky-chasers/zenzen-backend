const Imgur = require('../imgur-api/imgur');
const RandomString = require('randomstring');
const supabasePost = require('../supabase/supabase-post');
const { StatusCodes } = require('http-status-codes');
const { request, response } = require('express');

const createPost = async (request, response) => {
    try {
        const post = request.body.post;
        const imageLinks = await _uploadImageToImgur(post);        
        const insertPostResponse = await _insertPost(post);

        let postCopy = _buildResponse(post, imageLinks, insertPostResponse);

        await _insertImages(postCopy);
        console.log('Transaction done!');
        response.send(postCopy);
    }catch(reason) {
        response.sendStatus(StatusCodes.BAD_REQUEST).send(reason);
        return;
    }
    
}

const _buildResponse = (post, imageLinks, insertPostResponse) => {
    return {
        ...post,
        images: imageLinks,
        id : insertPostResponse.id,
        dateCreated: insertPostResponse.inserted_at
    };
}

const _insertPost = async(post) => {

    const response = await supabasePost.insertPost(post);

    if(response.status === 201) {
        return response.data[0];
    } else {
        throw `Something went wrong while inserting a post.`;
    }

}

const _insertImages = async(post) => {

    for(let image of post.images) {
        const response = await supabasePost.insertImage(image, post.id);

        if(response.status !== 201) {
            throw `Something went wrong while inserting an image.`;
        }
    }

}

const _uploadImageToImgur = async (post) => {
    if(post.images === undefined) {
        console.info(`There are no images to upload.`);
        return [];
    }

    const imageLinks = [];

    for (let image of post.images) {
        let imgurResult;
        const fileName = RandomString.generate(10);
        
        console.info(`Uploading image ${fileName} to imgur.com`);
        
        try {
            imgurResult = await Imgur.uploadBase64(image, fileName);
        
            const { data } = imgurResult;
            imageLinks.push(data.link);
        } catch (reason) {
            throw `Error occurred while uploading image with fileName: ${fileName}`;
        }
    }

    return imageLinks;
}

module.exports.createPost = createPost;