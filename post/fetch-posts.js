const supabasePost = require('../supabase/supabase-post');
const { fetchImages } = require('../image/fetch-images');
const { buildOutput } = require('./post-output-builder');
const { StatusCodes } = require('http-status-codes');

const fetchPosts = async (request, response) => {
    
    try {
        const {start, end} = request.query;
        const rawFetchPostsResponse = await supabasePost.fetchPosts(start, end);
        const rawCountPostsResponse = await supabasePost.countAllPosts();
        const postsCount = rawCountPostsResponse.count;

        const output = await _buildResponse(rawFetchPostsResponse, postsCount);
        response.send(output);
    }catch(reason) {
        response.sendStatus(StatusCodes.BAD_REQUEST).send(reason);
        return;
    }
}

const _buildResponse = async (rawPostResponse, count) => {
    const posts = rawPostResponse.data;

    const dataOutput = [];

    for(let post of posts) {
        const imageLinks = await fetchImages(post.id);
        const postOutput = buildOutput(post, imageLinks);    
        dataOutput.push(postOutput);
    }

    const response = {
        data: dataOutput,
        totalCount: count
    } 

    return response;
}

module.exports.fetchPosts = fetchPosts;