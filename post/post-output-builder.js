const buildOutput = (post, imageLinks) => {
    return {
        "id": post.id,
        "insertedAt": post.inserted_at,
        "updatedAt": post.updated_at,
        "title": post.title,
        "country": post.country,
        "city": post.city,
        "description": post.description,
        "emailAddress": post.email_address,
        "phoneNumber": post.phone_number,
        "postType": post.post_type,
        "contactName": post.contact_name,
        "images": imageLinks
    };
}

module.exports.buildOutput = buildOutput;