const { default: axios } = require('axios');
const { ImgurClient } = require('imgur')

const initialize = async () => {
    imgurClient = new ImgurClient({
        clientId: process.env.IMGUR_CLIENT_ID,
        clientSecret: process.env.IMGUR_SECRET,
        refreshToken: process.env.IMGUR_REFRESH_TOKEN,
    });

    console.info('Imgur initialized.');
}

const uploadBase64 = async (bas64String, fileName) => {
    let uploadResponse;
    try {
    uploadResponse = await imgurClient.upload({
        title: fileName,
        image: sanitizeBase64String(bas64String),
        type: 'base64'
    });
    } catch (reason) {
        console.error('upload failed', reason);
        return;
    }
    
    return uploadResponse;
};

const sanitizeBase64String = (dataURL) => {
    return dataURL.replace(/^data:image\/(png|jpg|gif|jpeg);base64,/, '');
}

module.exports.uploadBase64 = uploadBase64;
module.exports.initialize = initialize;