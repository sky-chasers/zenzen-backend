
const supabase = require('./supabase');

const insertPost = async (post) => {
    console.log(`Inserting post with title: ${post.title}`);

    return await supabase
    .getClient()
    .from('post')
    .insert([{
        title: post.title,
        post_type: post.postType,
        country: post.country,
        city: post.city,
        description: post.description,
        email_address: post.emailAddress,
        phone_number: post.phoneNumber,
        contact_name: post.contactName
    }])
    .select();
};

const insertImage = async (imageLink, postId) => {
    console.log(`Inserting image ${imageLink} with post id: ${postId}`);

    return await supabase
    .getClient()
    .from('image')
    .insert([{
        link: imageLink,
        post_id: postId
    }])
    .select();
}

const fetchPostById = async(postId) => {
    console.log(`Fetching post by id: ${postId}`);

    return await supabase
    .getClient()
    .from('post')
    .select('*')
    .eq('id', postId); 
}

const fetchPosts = async (start, end) => {
    console.log(`Fetching all posts...`);

    return await supabase
    .getClient()
    .from('post')
    .select('*')
    .order('inserted_at', { ascending: false })
    .range(start, end);
}

const countAllPosts = async() => {
    console.log(`Counting all posts.`);
    
    return await supabase
    .getClient()
    .from('post')
    .select('id', {count: 'exact'});
}

module.exports.insertPost = insertPost;
module.exports.insertImage = insertImage;
module.exports.fetchPosts = fetchPosts;
module.exports.countAllPosts = countAllPosts;
module.exports.fetchPostById = fetchPostById;