const supabase = require('./supabase');

const fetchImagesWithPostId = async (postId) => {
    console.log(`Fetching all images with post id: ${postId}...`);

    return await supabase
    .getClient()
    .from('image')
    .select('*')
    .eq('post_id', postId);
}

module.exports.fetchImagesWithPostId = fetchImagesWithPostId;