const {createClient} = require("@supabase/supabase-js");

let supabaseClient;

const initialize = async () => {
    supabaseClient = createClient(process.env.SUPABASE_URL, process.env.SUPABASE_KEY);

    console.info('supabase initialized.');
}

const getClient = () => {
    return supabaseClient;
}

module.exports.initialize = initialize;
module.exports.getClient = getClient;
